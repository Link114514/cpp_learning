#define  _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include <assert.h>
#include <iostream>
namespace kp {
	
	class String
	{
	public:
	/*	String()
			:_str(new char[1])
			, _size(NULL)
			, _capacity(NULL)
		{
			_str[0] = '0';
		}*/
			String(const char* str="")//字符串自带'\0'
		{
			_size = (strlen(str));//加上'\0'
			_capacity = _size;
			_str = (new char[_capacity+1]);
			strcpy(_str, str);
		}
		~String() {
			delete[]_str;
			_str = nullptr;
			_capacity = NULL;
			_size = NULL;
		}
	/*	String(const String& s) //传统写法
		{
			_str = new char[s._capacity + 1];
			strcpy(_str,s._str );
			_size = s._size;
			_capacity = s._capacity;
		}*/
		String(const String& s) //现代写法
		{
			String tmp(s._str);
			swap(tmp);
		}
		String(const String&& s)//
		{
			String tmp(s._str);
			swap(tmp);
		}
		void swap(String& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}
		typedef char* iterator;
		typedef const char* const_iterator;//使用const来让指向的不能修改
		iterator begin()const {
			return _str;
		}
		iterator end()const {
			return _str + _size;
		}
		size_t getsize() {
			return _size;
		}
		size_t getcapacity() {
			return _capacity;
		}
		const char* c_str()const;

		const char& operator[] (size_t pos)const
		{
			assert(pos <= _size);
			return _str[pos];
		}
		char& operator[](size_t pos) 
		{
			assert(pos <= _size);
			return _str[pos];
		}
	
		String& operator=(String s);
		String& operator=(String&& s);
		String& operator+=(char ch)
		{
			 pushback(ch);
			 return *this;
		}
		String& operator+=(const char*str)
		{
			pushback(str);
			return *this;
		}
		void clear()
		{
			_size = 0;
			_str[0] = '\0';
		}
		void reserve(size_t n);
		void pushback(char ch);
		void pushback(const char* str);
		void insert(size_t pos, char ch);
		void Print();
		size_t find(const char* str, size_t pos = 0);
	private:
		size_t _size;
		size_t _capacity;
		char* _str;//初始化列表先后顺序要注意
	    static const size_t npos = -1;
	};

	std::istream& operator>>(std::istream& in, String& s);
	std::ostream& operator<<(std::ostream& out, String& s);
}