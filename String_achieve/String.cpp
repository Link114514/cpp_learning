#include "String.h"
namespace kp {
	size_t String::find(const char* str, size_t pos )
	{
		const char* ptr = strstr(_str + pos, str);
		if (ptr == nullptr)
		{
			return npos;
		}
		else
		{
			return ptr - _str;//指针相减即可得出
		}
	}

	void String::Print()
	{
		for (size_t i = 0; i < _size; i++)
		{
			std::cout << _str[i];
		}
	}

	void String::insert(size_t pos, char ch)
	{
		assert(pos <= _size);
		if (_size == _capacity)
		{
			size_t newcapacity = _capacity == 0 ? 4 : 2 * _capacity;
			reserve(newcapacity);
		}
		//int end = _size;
		//while (end>=(int)pos)//size_t和int比较会隐式类型转换
		//{
		//	_str[end + 1] = _str[end];
		//	--end;
		//}
		size_t end = _size + 1;
		while (end > pos)
		{
			_str[end] = _str[end - 1];
			--end;
		}
		_str[pos] = ch;
		_size++;
	}

	String& String:: operator=(String s) //!交换不能加别名 要交换临时拷贝
	{
		//if (this!=&s)
		//{
		//	char* tmp = new char[s._capacity + 1];//深拷贝 赋值 开新的 换旧的
		//	strcpy(tmp, _str);
		//	delete[]_str;
		//	_str = tmp;
		//	_size = s._size;
		//	_capacity = s._capacity;
		//}
		swap(s);
		return *this;
	}
	String& String::operator=(String&& s)//移动赋值
	{
		swap(s);
		return *this;
	}
	void String::pushback(const char* str)
	{
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}
		strcpy(_str + _size, str);
		_size += len;
	}

	void String::pushback(char ch)
	{
		if (_capacity == _size)
		{
			size_t newcapacity = _capacity == 0 ? 4 : 2 * _capacity;
			reserve(newcapacity);
		}
		_str[_size] = ch;
		_size++;
		_str[_size] = '\0';
	}

	void String::reserve(size_t n)
	{
		if (n > _capacity)
		{
			char* tmp = new char[n + 1];//存放'\0'
			strcpy(tmp, _str);//会拷贝'\0'
			delete(_str);
			_str = tmp;
			_capacity = n;
		}
	}

	std::istream& operator>>(std::istream& in, String& s)
	{
		s.clear();
		char buff[128];
		char ch = in.get();
		int i = 0;
		while (ch != '\0' && ch != '\n')
		{
			buff[i++] = ch;
			if (i == 127)
			{
				buff[127] = '\0';
				s += buff;
				i = 0;
			}
			ch = in.get();
		}
		if (i > 0)
		{
			buff[i] = '\0';
			s += buff;
		}
		return in;
	}
	std::ostream& operator<<(std::ostream& out, String& s)
	{
		for(auto ch:s)
		{
			out <<ch;
		}
		return out;
	}

}