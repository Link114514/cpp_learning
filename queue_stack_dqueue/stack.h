#pragma once
#include <deque>
#include <vector>
#include <iostream>
using namespace std;
template<class T,class Con=deque<T>>
class stack 
{
public:
	void Push(const T& x) 
	{
		_c.push_back(x);
	}
	void Pop() 
	{
		_c.pop_back();
	}
	const T& Top() const 
	{
		return _c.back();
	}
	const size_t size() const {
		return _c.size();
	}
	bool empty()const 
	{
		return _c.empty();
	}
private:
	Con _c;
};