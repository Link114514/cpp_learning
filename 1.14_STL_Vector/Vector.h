#pragma once
#include <iostream>
#include <assert.h>
namespace kp {
	// 适配器 -- 复用
	template<class iterator, class Ref, class Ptr>
	struct Reverse_iterator
	{
		typedef Reverse_iterator<iterator, Ref, Ptr> self;
		Reverse_iterator(iterator it)
			:_it(it)
		{}
		Reverse_iterator(Reverse_iterator& s)
			:_it(s)
		{
		}
		self& operator++()
		{
			--_it;//调用正向迭代器的--,这里正向迭代器就是原生指针
			return *this;
		}
		self operator++(int)
		{
			_it--;
			return *this;
		}
		self& operator--()
		{
			++_it;
			return *this;
		}
		self operator--(int)
		{
			_it++;
			return *this;
		}
		Ref operator*()
		{
			Ref tmp = *this->_it;
			return tmp;
		}
		bool operator!=(const self& it)
		{
			return _it != it._it;
		}
		bool operator==(const self& it)
		{
			return _it == it._it;
		}

	private:
		iterator _it;
	};
	template <class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		typedef Reverse_iterator<iterator, T&, T*> Reverse_iterator;
		vector()
			:_start(nullptr)
			,_finish(nullptr)
			,_endofstorage(nullptr)
		{}
		template <class LT>
		vector(LT first, LT last)
		{
			while (first!=last)
			{
				push_back(*first);
				++first;
			}
		}
		vector(const vector<T>& v)
		{
			reserve(v.capacity());
			for (const auto &ch:v)
			{
				push_back(ch);  
			}
		}
		vector(size_t n, const T& val = T())
		{
			resize(n, val);
		}
		vector(int n, const T& val = T())
		{
			resize(n, val);
		}
		vector<T>& operator=(vector<T>& v)
		{ 
			swap(v);
			return *this;
		}
		void swap(vector<T>& v)
		{
			std::swap(_start, v->_start);
			std::swap(_finish, v->_finish);
			std::swap(_endofstorage, v->_endofstorage);
		}
		size_t size() const
		{
			return _finish-_start;
		}
		size_t capacity()const
		{
			return _endofstorage - _start;
		}
		iterator end()
		{
			return _finish;
		}
		iterator begin()
		{
			return _start;
		}	
		iterator rbegin()
		{
			return _finish;
		}
		iterator rend()
		{
			return _start;
		}
		iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos <_finish);
			iterator it = pos + 1;
			while (it< _finish)
			{
				*(it - 1) = *it;
				++it;
			}
			_finish--;
			return pos;//返回新的迭代器1.防止位置已经变动2.防止野指针
		}
		iterator insert(iterator pos, const T& x)
		{
			assert(pos >= _start && pos <= _finish);
			if (_finish==_endofstorage)
			{
				size_t len = pos - _start;
				reserve(capacity() == 0 ? 4 : 2 * capacity());
				pos = len + _start;
			}
			iterator end = _finish - 1;
			while (end>=pos)
			{
				*(end + 1) = *end;
				--end; 
			}
			_finish = x;
			++_finish;
			return pos;
		}
		void push_back(const T& X)
		{
			if (_finish==_endofstorage)
			{
				size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
				reserve(newcapacity);
			}
			*_finish = X;
			++_finish;
		}
		void resize(size_t n,T val= T())
		{
			if (n<size())
			{
				_finish = _start+n;
 			}
			if (n > size())
			{
				reserve(n);
				while (_finish < _start + n)
				{
					*_finish = val;
					++_finish;
				}
			}
		}
		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t  old = size();//提前记录
				T* tmp = new T[n];
				if (_start)
				{
					//memcpy(tmp, _start, n * sizeof(T));//此处自定义类型深拷贝会引发会浅拷贝,string的_str还指向原来的_start,调用内置类型的析构,释放空间 ,导致迭代器野指针 
					for (size_t i = 0; i < old; i++)
					{
						tmp[i] = _start[i];//单独给每个内置类型深拷贝
					}
					delete[]_start;
				}
				_start = tmp;
				_finish = _start + old;
				_endofstorage = _start + n;
			}
		}

	private:
		iterator _start;
		iterator _finish;
		iterator _endofstorage;
	};
}