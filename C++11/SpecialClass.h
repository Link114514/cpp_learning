#pragma once
#include <vector>
using namespace std;
template<class T>
class Heaponly
{
public:
	template<class ...Args>
	static Heaponly* CreatObj(Args&& ...arg)
	{
		return new Heaponly(arg...);//只在堆上
	}
	Heaponly(const Heaponly& h) = delete;
	Heaponly& operator=(const Heaponly& h) = delete;
private:
	template<class ...Args>
	Heaponly(T val)
		:_val(val)
	{}
	T _val;
	vector<int>_a;
};

//封
template<class T>
class Stackonly
{
public:
	template<class ...Args>
	static Stackonly CreatObj(Args&& ...arg)
	{
		return  Stackonly(arg...);//只在栈上
	}
	/*void Destory()
	{
		delete this;
	}*/
	Stackonly& operator=(const Stackonly& ) = delete;
	void* operator new(size_t i) = delete;
private:
	Stackonly(T val)
		:_val(val)
	{}
	/*~Stackonly()
	{
		cout << "delete" << endl;
	}*/
	T _val;
	vector<int>_a;
};

//调用
template<class T>
class Stackonly2
{
public:
	Stackonly2(T val)
		:_val(val)
	{}
	void Destory()
	{
		delete this;//先析构再free
	}
//	Stackonly2(const Stackonly2& h) = delete;
//	Stackonly2& operator=(const Stackonly2&) = delete;
	void* operator new(size_t i) = delete;
private:
	~Stackonly2()
	{
		cout << "delete" << endl;
	}
	T _val;
	vector<int>_a;
};


class Heaponly2
{
public:
	Heaponly2()
		:_val(1)
	{}
	Heaponly2(int val)
		:_val(val)
	{}
	void Destory()
	{
		delete this;
	}
private:
	~Heaponly2()
	{
		cout << "delete" << endl;
	}
	int _val;
};
