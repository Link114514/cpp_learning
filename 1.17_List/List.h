#pragma once
#include <iostream>
#include <assert.h>
namespace kp{
	template<class T>
	struct ListNode
	{
		ListNode<T>* _next;
		ListNode<T>* _prev;
		T _val;
		ListNode(const T& x=T())
			:_next(nullptr)
			, _prev(nullptr)
			,_val(x)
		{
		}
	};
		template <class T,class Ref>
		struct _list_iterator
		{
			typedef ListNode<T> Node;
			typedef _list_iterator<T,Ref> self;
			Node* _node;

			_list_iterator(Node*node)
				:_node(node)
			{}
			_list_iterator(const Node*node)
				:_node(node)
			{
			}
			self& operator++()
			{
				_node = _node->_next;
				return *this;
			}
			self operator++(int)
			{
				self tmp(*this);
				_node = _node->_next;
				return tmp;
			}
			self& operator--()
			{
				_node = _node->_prev;
				return *this;
			}
			self operator--(int)
			{
				self tmp(*this);
				_node = _node->_next;
				return tmp;
			}
			Ref operator*()
			{
				return _node->_val;
			}
			bool operator!=(const self& s)
			{
				return _node != s._node;
			}
			bool operator==(const self& s)
			{
				return _node == s._node;
			}
		};
		template<class _list_iterator,class Ref,class Ptr>
		class reverse_iterator
		{
		public:
			typedef  reverse_iterator <_list_iterator, Ref, Ptr>self;
			reverse_iterator(_list_iterator it)
				:cur(it)
			{}
			self& operator++()
			{
				--cur;
				return *this;
			}
			self& operator--()
			{
				++cur;
				return *this;
			}
			Ref& operator*()
			{
				_list_iterator tmp(cur);
				--tmp;//因为是取前一个迭代器的值
				return *tmp;
			}
			bool operator!=(const self&s)
			{
				return cur != s.cur;
			}
			bool operator==(const self& s)
			{
				return cur == s.cur;
			}
		private:
			_list_iterator cur;
		};

	template <class T>
	class List
	{
		typedef ListNode<T> Node;

	public:
		typedef _list_iterator<T,T&> iterator;
		typedef _list_iterator<T,const T&> const_iterator;
		typedef reverse_iterator<iterator, const T&, const T*>const_reverse_iterator;
		typedef reverse_iterator<iterator, T&, T*>reverse_iterator;

		iterator begin()
		{
			return iterator(_head->_next);
		}

		const_iterator const_begin() const
		{
			return const_iterator(_head->_next);
		}
		iterator end()
		{
			return iterator(_head);
		}
		const_iterator const_end() const
		{
			return const_iterator(_head);
		}
		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}
		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}	    
		iterator erase(iterator pos)
		{
			assert(pos != end());
			Node* pcur =pos._node;
			Node* newprev = pcur->_prev;
			Node* newnext= pcur->_next;
			newprev->_next = newnext;
			newnext->_prev = newprev;
			delete pcur;
			pcur = nullptr;

			return newnext;//删除之前的那个
		}
		void Empty_Init()
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;
		}
		List()
		{
			Empty_Init();
		}
		List(const T&x)
		{
			_head = new Node(x);
			_head->_next = _head;
			_head->_prev = _head;
		}
		List(List<T>& lt)
		{
			Empty_Init();
			for (const auto& ch:lt)
			{
				Push_back(ch);
			}
		}
		~List() 
		{
			clear();
			delete _head;
			_head = nullptr;
		}
	
		void Push_back(const T& x)
		{
			/*Node* newnode = new Node(x);
			Node* _tail = _head->_prev;

			_tail->_next = newnode;
			newnode->_next = _head;
			newnode->_prev = _tail;
			_head->_prev = newnode;*/
			insert(end(), x);
		}
		void Push_front(const T& x)
		{
			insert(begin(), x);
		}
		void Pop_Back()
		{
			erase(--end());//end是哨兵位
		}
		void Pop_front()
		{
			erase(begin());
		}
		iterator insert(iterator pos, const T& x)//之前插入
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			//prev newnode cur
			Node* newnode = new Node(x);
			prev->_next = newnode;
			newnode->_next = cur;
			newnode->_prev = prev;
			cur->_prev = newnode;

			return newnode;
		}
		void clear()//复用
		{
			iterator it = begin();
			while (it!=end())
			{
				it = erase(it);
			}
		}
	private:
		Node* _head;
	};

}

