#include <thread>
#include <vector>
#include <mutex>
#include <queue>
#include <string>
#include <iostream>
#include <condition_variable>
//#include "LOG.hpp"

class threaddata
{
	threaddata(std::string threadname,int tid)
		:_threadname(threadname)
		,_tid(tid)
	{}
	~threaddata() = default;
private:
	std::string _threadname;
	int _tid;
};

template<class T>
class threadpool
{
private:
	void queuelock()
	{
		_queue_mutex.lock();
	}
	void queueunlock()
	{
		_queue_mutex.unlock();
	}
	void WakeThread()
	{
		_task_con.notify_one();
	}
	void WakeAllTthread()
	{
		_task_con.notify_all();
	}
	void Start_WakeAll()
	{
		_start_con.notify_all();
	}
	void Start_WakeOne()
	{
		_start_con.notify_one();
	}
	
public:
	threadpool(int threadnum)
		:_threadnum(threadnum),_waitnum(0),_runningnum(0),_Isrunning(false)
	{
	}
	~threadpool()
	{
		_start_con.~condition_variable();
		_start_mutex.~_Mutex_base();
		_queue_mutex.~_Mutex_base();
		_start_con.~condition_variable();
	}
	void HanderTask()
	{
		{
			std::unique_lock<std::mutex> start_lock(_start_mutex);
			while (!_Isrunning) {//
				_start_con.wait(start_lock);//?bug
			}
		}
		while (true)
		{
			queuelock();
			while (_Taskqueue.empty() && !_Isrunning)
			{
				queueunlock();
				//log
				break;
			}
			while (_Taskqueue.empty() && _Isrunning)
			{
				++_waitnum;
				SleepThread();
				//log
				--_waitnum;
			}

			T task = _Taskqueue.front();
			_Taskqueue.pop();
			//log
			if (task())
			{
				std::cout << "mission success" << std::endl;
			} // ִ��
			else
			{
				std::cout << "mission fail" << std::endl;
			}
			queueunlock();
		}
	}
	bool Enqueue(const T& task)
	{
		bool ret = false;
		queuelock();
		if (_Isrunning)
		{
			++_waitnum;
			_Taskqueue.push(task);
			if (_waitnum > 0)
			{
				WakeThread();
				--_waitnum;
			}
			ret = true;
		}
		queueunlock();
		return ret;
	}
	void Startthread()
	{
		_Isrunning = true;
		while (!_Taskqueue.empty())
		{
			Start_WakeOne();
		}
	}
	void Initpool()
	{
		for (int i = 0; i < _threadnum; i++)
		{
			std::string name = new std::string("Thread:" + i);
			int tid = _threadpool[i].get_id();
			_datapool.push_back({ name,tid });
			_threadpool.emplace_back(std::thread(threadpool::HanderTask));
		}
		_Isrunning = true;
		Start_WakeAll();
	}
	static threadpool<T>* Getinstance()
	{
		if (_instance == nullptr)
		{
			std::lock_guard<std::mutex> guard(_static_lock);
			if (_instance == nullptr)
			{
				_instance = new threadpool<T>();
				_instance->Initpool();
				_instance->Start();
				return _instance;
			}
		}
		return _instance;
	}
	void Start()
	{
		Start_WakeAll();
	}
	void Stop()
	{
		queuelock();
		_Isrunning = false;
		WakeAllTthread();
		//log
		Start_WakeAll();
		queueunlock();
	}
	void Join()
	{
		for (auto &thread : _threadpool)
		{
			thread.join();
		}
	}

private:
	int _threadnum;
	bool _Isrunning;

	std::vector<std::thread> _threadpool;
	std::vector<threaddata> _datapool;
	std::queue<T> _Taskqueue;
	-
	std::mutex _queue_mutex;
	std::mutex _start_mutex;


	std::condition_variable _task_con;
	std::condition_variable _start_con;


	int _waitnum;
	int _runningnum;

/////////////////////////////////////////////////����
	static threadpool<T>* _instance;
	static std::mutex _static_lock;
};

template<class T>
threadpool<T>* threadpool<T>::_instance = nullptr;

��ʼ
|
v
��ʼ�� temp = 0
|
v
�ж� count > 0 ?
|
+----��----->���� temp
|
��
|
v
�ж� flag == 0 ?
|
+----��----->temp = count * 2
|                |
|                v
| ����ѭ��
|
��
|
v
�ж� flag == 1 ?
|
+----��----->temp = temp * 8
|
��
|
v
temp = temp + 200
|
v
count = count - 1
|
v
���ز��� �ж� count > 0 ?