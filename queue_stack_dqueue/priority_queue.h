 #define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <string>
using namespace std;
namespace kp {
	template<class T>
	class less
	{
		bool operator()(const T& x,const T& y)
		{
			return x > y;
		}
	};
	template<class T>
	class greater
	{
		bool operator()(const T& x,const T& y)
		{
			return x < y;
		}
	};

	template<class T,class Container=vector<T>,class Compare=less<T>>
	class priority_queue//���
	{
	public:
		void adjustup(T child)
		{
			T parent = (child - 1) / 2;
			while (child>0)
			{
				if (_comp(_con[child], _con[parent]))
				{
					swap(_con[child], _con[parent]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}
		void adjustdown(T parent)
		{
			T child = parent * 2 + 1;
			while (child<_con.size())
			{
				if (child+1<_con.size()&&_comp(_con[child+1],_con[child]))
				{
					++child;
				}
				if (_comp(_con[child],_con[parent]))
				{
					swap(_con[child], _con[parent]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}
		const T& top()
		{
			return _con[0];
		}
		void push(const T& x)
		{
			_con.push_back(x);
			adjustup(_con.size() - 1);
		}
		void pop()
		{
			swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			adjustdown(0);
		}
		size_t size()
		{
			return _con.size();
		}
		bool empty() const
		{
			return _con.empty();
		}
	private:
		Container _con;
		Compare _comp;
	};
}