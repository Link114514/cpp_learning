 #define  _CRT_SECURE_NO_WARNINGS 1
//#include "threadpool.hpp"
#include <thread>
#include <mutex>
#include <Windows.h>
#include <iostream>
std::mutex mutex;
bool change = false;
void test1()
{
	while (true)
	{
		mutex.lock();
		if (change)
		{
			mutex.unlock();
			continue;
		}
		Sleep(100);
		std::cout << "1" << std::endl;
		change = true;
		mutex.unlock();

	}
}

void test2()
{
	while (true)
	{
		mutex.lock();
		if (!change)
		{
			mutex.unlock();
			continue;
		}
		Sleep(100);
		std::cout << "2" << std::endl;
		change = false;
		mutex.unlock();
	}
}


int main()
{
	std::thread t1(test1);
	std::thread t2(test2);
	
	t1.join();
	t2.join();


	
	return 0;
}