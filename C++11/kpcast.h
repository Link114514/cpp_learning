#pragma once
void test()
{
	int a = 10;
	const int c = 10;
	int* b = static_cast<int*>(&a);//static_cast用于非多态类型的转换（静态转换），编译器隐式执行的任何类型转换都可用static_cast，但它不能用于两个不相关的类型进行转换
	int* b = reinterpret_cast<int*>(a); //用于将一种类型转换为另一种不同的类型
	int* d = const_cast<int*>(&c);//去const属性
	*d +=11;
	class A
	{
		virtual void fun();
	private:
		int _val;
	};
	class B:A
	{
	private:
		const char* _Name;
	};
	A* pa ;
	B* a = dynamic_cast<B*>(pa);//必须在多态情况下
	B* pb;
	B* aa = dynamic_cast<B*>(pb);//父类转换成子类
}
