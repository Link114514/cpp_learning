#define  _CRT_SECURE_NO_WARNINGS 1

//int add(int a, int b)
//{
//    return a + b;
//}
//struct Ad
//{
//    int operator()(int a, int b) {
//        return a + b;
//    }
//};
//class func
//{//非静态成员函数需要对象的指针或者对象去进行调用
//public:
//    func() = default;
//    int AD(int a, int b)
//    {
//        return a + b;
//    }
//};
//class A
//{
//    A(int a=10)
//        :_a(a)
//    { }
//    A() = default;
//private:
//    int _a;
//
//};
//void tfunc(int a)
//{
//    if (a == 1)
//    {
//        cout << __LINE__ << endl;
//        throw "错误";
//    }
//    else
//    {
//        cout << "正常" << endl;
//    }
//}
//int main()
//{
//    
//    //function<int(int, int)>add1 = add;
//    //function<int(int, int)>add2 = Ad();
//    //function<int(int, int)>add3 = [=](int a, int b) {return a + b; };//lambda
//    //function<int(func, int, int)>add4 = &func::AD;//传对象的指针
//    /////////////////////////////////////////////////////////////
//    //int a = 1, b = 2;
//    //auto f = [=, &b, &a](int c)->int{return c += a + b;};//??? 狗屎 换了个语言了
//    //cout << f(4);
//
//    try
//    {
//        tfunc(1);
//    }
//    catch(exception&e)
//    {
//        cout << e.what() << endl;
//    }
//
//    
//    return 0;
//}
//class Exception
//{
//public:
//	Exception(const string& errmsg, int id)
//		:_errmsg(errmsg)
//		, _id(id)
//	{}
//	virtual string what() const
//	{
//		return _errmsg;
//	}
//protected:
//	string _errmsg;
//	int _id;
//};
//class SqlException : public Exception
//{
//public:
//	SqlException(const string& errmsg, int id, const string& sql)
//		:Exception(errmsg, id)
//		, _sql(sql)
//	{}
//	virtual string what() const
//	{
//		string str = "SqlException:";
//		str += _errmsg;
//		str += "->";
//		str += _sql;
//		return str;
//	}
//private:
//	const string _sql;
//};
//class CacheException : public Exception
//{
//public:
//	CacheException(const string& errmsg, int id)
//		:Exception(errmsg, id)
//	{}
//	virtual string what() const
//	{
//		string str = "CacheException:";
//		str += _errmsg;
//		return str;
//	}
//};
//class HttpServerException : public Exception
//{
//public:
//	HttpServerException(const string& errmsg, int id, const string& type)
//		:Exception(errmsg, id)
//		, _type(type)
//	{}
//	virtual string what() const
//	{
//		string str = "HttpServerException:";
//		str += _type;
//		str += ":";
//		str += _errmsg;
//		return str;
//	}
//private:
//	const string _type;
//};
//void SQLMgr()
//{
//	srand(time(0));
//	if (rand() % 7 == 0)
//	{
//		throw SqlException("权限不足", 100, "select * from name = '张三'");
//	}
//	//throw "xxxxxx";
//}
//void CacheMgr()
//{
//	srand(time(0));
//	if (rand() % 5 == 0)
//	{
//		throw CacheException("权限不足", 100);
//	}
//	else if (rand() % 6 == 0)
//	{
//		throw CacheException("数据不存在", 101);
//	}
//	SQLMgr();
//}
//void HttpServer()
//{
//	// ...
//	srand(time(0));
//	if (rand() % 3 == 0)
//	{
//		throw HttpServerException("请求资源不存在", 100, "get");
//	}
//	else if (rand() % 4 == 0)
//	{
//		throw HttpServerException("权限不足", 101, "post");
//	}
//	CacheMgr();
//}
//int main()
//{
//	ios::sync_with_stdio();
//	cin.tie(0);
//	cout.tie(0);
//	while (1)
//	{
//		this_thread::sleep_for(chrono::seconds(1));
//		try {
//			HttpServer();
//		}
//			catch (const Exception& e) // 这里捕获父类对象就可以
//		{
//			// 多态
//			cout << e.what() << endl;
//		}
//		catch (...)
//		{
//			cout << "Unkown Exception" << endl;
//		}
//	}
//	return 0;
//}
//#include "Kpmemmory.h"
//#include "bitsRAII.h"
//#include "SpecialClass.h"
//#include "singleton.h"
//
//
//int main()
//{
//	/*Heaponly<int>* h1 = Heaponly<int>::CreatObj(1);
//	Stackonly<int> s1 = Stackonly<int>::CreatObj(1);
//	shared_ptr<Heaponly2> ptr(new Heaponly2, [](Heaponly2* ptr) {ptr->Destory(); });//lambda定制删除
//	kp::shared_ptr<Heaponly2> ptr1(new Heaponly2, [](Heaponly2* ptr) {ptr->Destory(); });//lambda定制删除
//	*/
//	/*vector<string >s({ "123" });
//	lazy::Singleton::GetInstance(1, 2, s);
//	lazy::Singleton::GetInstance()->Print();
//	lazy::Singleton::GetInstance()->Addstr("123");
//	lazy::Singleton::GetInstance()->Print();*/
//	int a = 10;
//	int& b = a;
//	cout << a <<" " << b << endl;
//	cout << &a << " " << &b << endl;
//
//	return 0;
//}
