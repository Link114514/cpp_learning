#pragma once
#include <iostream>
#include <functional>
using namespace std;
namespace kp {
	//template<class T>
	//struct  DeleteArray//定制删除器
	//{
	//	void operator()(T*ptr)
	//	{
	//		delete[] ptr;
	//	}
	//};
	template <class T>
	class auto_ptr
	{
	public:
		auto_ptr(T*ptr)
			:_ptr(ptr)
		{}
		~auto_ptr()
		{
			delete _ptr;//同一对象释放两次
		}
		auto_ptr(auto_ptr<T>& sp)
			:_ptr(sp._ptr)
		{
			// 管理权转移
			sp._ptr = nullptr;
		}
		auto_ptr<T>& operator=(auto_ptr<T>& ap)
		{
			// 检测是否为自己给自己赋值
			if (this != &ap)
			{
				// 释放当前对象中资源
				if (_ptr)
					delete _ptr;
				// 转移ap中资源到当前对象中
				_ptr = ap._ptr;
				ap._ptr = NULL;
			}
			return *this;
		}
		T* operator->()
		{
			return _ptr;
		}
		T& operator*()
		{
			return *_ptr;
		}
	private:
		T* _ptr;
	};

	template <class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}
		~unique_ptr()
		{
			delete _ptr;
		}
		unique_ptr<T>& operator=(const unique_ptr<T>& s) = delete;
		unique_ptr(const unique_ptr<T>& s) = delete;
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};
	
	template <class T>
	class shared_ptr
	{
	public:
		template<class D>
		shared_ptr(T* ptr, D del)
			: _ptr(ptr)
			, _pcount(new int(1))
			, _del(del)
		{}

		shared_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			,_pcount(new int(1))
		{}

		shared_ptr(const shared_ptr<T>& s)
		{
			_ptr = s._ptr;
			_pcount = s._pcount;
			++(*_pcount);
		}
		~shared_ptr()
		{
			release();
		}
		shared_ptr<T>& operator=(const shared_ptr<T>& s)
		{
			if (_ptr !=s._ptr)
			{
				release();
				_ptr = s._ptr;
				_pcount = s._pcount;
				++(*_pcount);
			}
			return *this;
		}
		int use_count()
		{
			return *_pcount;
		}
		T* operator->()
		{
			return _ptr;
		}
		T& operator*()
		{
			return *_ptr;
		}
		T* getptr()const
		{
			return _ptr;
		}
	private:
		void release()
		{
			if (--(*_pcount) == 0)
			{
				cout << "delete" << endl;
				_del(_ptr);
				delete _pcount;
			}
		}
		T* _ptr;
		int* _pcount;//  引用计数 同一数据指向同一个计数
		function<void(T*)> _del = [](T* ptr) {delete ptr; };
	};
	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr()//weak_ptr不去增加也不减少引用计数
			:_ptr(nullptr)
		{}
		weak_ptr<T>& operator=(const shared_ptr<T>& s)
		{
			_ptr = s.getptr();
			return *this;
		}
		weak_ptr(const shared_ptr<T>& s)
		{
			_ptr = s.getptr();
		}
	private:
		T* _ptr;
	};
};


template<class T>
struct ListNode
{
	//kp::shared_ptr<ListNode>_prev;
	//kp::shared_ptr<ListNode>_next;//内存泄漏 
	/* 当发生 s->next=p p->prev=s时 循环引用计数无法得到释放 s被q->prev管 q被s->next管理*/
	kp::weak_ptr<ListNode>_prev;//采用weak_ptr避开循环引用计数
	kp::weak_ptr<ListNode>_next;
	T _val;
	ListNode(T val=0)
		:_val(val)
	{}
};