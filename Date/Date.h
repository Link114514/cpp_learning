#pragma once
#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

class Date
{
public:
	Date(int year = 1, int month = 1, int day = 1);
	bool operator<(const Date& d);
	bool operator<=(const Date& d);
	bool operator>(const Date& d);
	bool operator>=(const Date& d);
	bool operator==(const Date& d);
	bool operator!=(const Date& d);

	// d1 + 100
	Date& operator+=(int day);
	Date operator+(int day);
	// d1 - 100
	Date operator-(int day);
	Date& operator-=(int day);

	// ++d1
	Date& operator++();
	// 特殊处理：解决语法逻辑不自洽，自相矛盾的问题
	// d1++
	// 为了跟前置++区分，强行增加一个int形参，够成重载区分
	Date operator++(int);

	Date operator--(int);
	Date& operator--();

	// d1 - d2
	int operator-(const Date& d);

	// 本质就是inline
	int GetMonthDay(int year, int month)
	{
		assert(month > 0 && month < 13);
		static int monthDays[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		// 365   自转  公转  365 5+h
		// 366
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
		{
			return 29;
		}

		return monthDays[month];
	}

	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};
