#pragma once
#include <deque>
using namespace std;
namespace kp {
	template<class T, class Continer = deque<T>>
	class queue
	{
	public:
		void push(const T& x)
		{
			_con.push_back(x);
		}
		void pop()
		{
			_con.pop_front();
		}
		T& front()
		{
			return _con.front();
		}
		T& back()
		{
			return _con.back();
		}
		const T& front() const
		{
			return  _con.front();
		}
		const T& back() const
		{
			return   _con.back();
		}
		bool empty()const
		{
			return _con.empty();
		}
		size_t size()const
		{
			return _con.size();
		}
	private:
		Continer _con;
	};
}