#include "sports.h"
std::string filename = "projects.txt";
bool Isrunning = true;
void wait_for_keypress() {
	printf("输入任意键返回.........\n");
	// 清除输入缓冲区
	while (getchar() != '\n');
	// 等待用户按下一个键
	getchar();
}

void menu() {
	cout << "当前默认数据保存在当前目录的projects.txt下 是否要修改(1/0)" << endl;
	int change;
	cin >> change;
	if (change == 1)
	{
		cout << "输入要保存到的文件名" << endl;
		cin >> filename;
	}
	SportsMeetingManager sm(filename);

	int choice;
	do {
		system("cls");
		cout << "==========运动会成绩管理================\n";
		cout << "         1. 录入项目成绩信息\n";
		cout << "         2. 统计学校积分\n";
		cout << "         3. 查询学校积分\n";
		cout << "         4. 查询学校项目信息\n";
		cout << "         5. 按学校总积分排序\n";
		cout << "         6. 查询项目信息\n";
		cout << "         7. 查询获奖项目最多的学校\n";
		cout << "         8. 项目信息保存\n";
		cout << "         0. 退出系统\n";
		cout << "====================================\n";
		cout << "请选择（1-8，0：退出）：";
		cin >> choice;

		switch (choice) {
		case 1: 
			sm.InputProjectResults();
			break;	
		case 2:	
			sm.CalculateScores();
			wait_for_keypress();
			break;	
		case 3: {
			int schoolID;
			cout << "输入学校编号：";
			cin >> schoolID;
			sm.FindSchoolScores(schoolID);
			wait_for_keypress();
			break;
		}
		case 4: {//防止编译器报错
			int schoolID;
			cout << "输入学校编号：";
			cin >> schoolID;
			sm.QuerySchoolProjects(schoolID);
			wait_for_keypress();
			break;
		}
		case 5:
			sm.SortSchoolsByTotalScore();
			wait_for_keypress();
			break;
		case 6: 
			int projectID;
			cout << "输入项目编号：";
			cin >> projectID;
			sm.FindProjectInfo(projectID);
			wait_for_keypress();
			break;
		case 7:
			sm.FindSchoolWithMostAwards();
			wait_for_keypress();
			break;
		case 8:
			sm.SaveProjectsToFile(filename);
			wait_for_keypress();
			break;
		case 0:
			cout << "退出系统\n";
			wait_for_keypress();
			break;
		default:
			cout << "无效的选择，请重新输入。\n";
		}
	} while (choice != 0);
}
int main()
{
	SportsMeetingManager sm(filename);
	// 添加学校信息
	sm.addSchool(1, "学校1");
	sm.addSchool(2, "学校2");
	sm.addSchool(3, "学校3");
	// 运行菜单
	menu();
	return 0;
	return 0;
}