#pragma once
#include <iostream>
#include <vector>
#include <string>
using namespace std;
namespace hunger{
	class Singleton
	{
	public:
		static Singleton* GetInstance()
		{
			return &_sint;
		}
		void Addstr(const string& s)
		{
			_vstr.push_back(s);
		}
		Singleton(Singleton const&) = delete;
		Singleton& operator=(Singleton const&) = delete;
	private:
		Singleton(int x = 0, int y = 0, const vector<string>& vstr = { "yyyyy","xxxx" })
			:_a(x)
			, _b(y)
			, _vstr(vstr)
		{}
		int _a;
		int _b;
		vector<string> _vstr;
		static Singleton _sint;//静态全局对象放进类域里
	};
	hunger::Singleton hunger::Singleton::_sint(1, 1, { "asd" });
};

namespace lazy
{
	class Singleton
	{
	public:
		static Singleton* GetInstance()//第一次调用的时候才创建对象
		{
			if (_sint == nullptr)
			{
				_sint = new Singleton;
			}
			return _sint;
		}
		static Singleton* GetInstance(int a,int b,vector<string>&s)//第一次调用的时候才创建对象
		{
			if (_sint == nullptr)
			{
				_sint = new Singleton(a,b,s);
			}
			return _sint;
		}


		void Print()
		{
			cout << _a << _b;
			for (auto& ch : _vstr)
			{
				cout << ch <<" ";
			}
			cout << endl;
		}

		void Addstr(const string& s)
		{
			_vstr.push_back(s);
		}

		static void DelInstance()
		{
			if (_sint)
			{
				delete _sint;
				_sint = nullptr;
			}
		}

		Singleton(Singleton const&) = delete;
		Singleton& operator=(Singleton const&) = delete;
	private:
		Singleton(int x = 0, int y = 0, const vector<string>& vstr = { "yyyyy","xxxx" })
			:_a(x)
			, _b(y)
			, _vstr(vstr)
		{}
		~Singleton()
		{
			cout << "delete" << endl;
		}
		// 想让一些数据，当前程序只有一份，那就可以把这些数据放到这个类里面
		// 再把这个类设计成单例，这个数据就只有一份了
		int _a;
		int _b;
		vector<string> _vstr;

		// 静态成员对象，不存在对象中，存在静态区，相当于全局的，定义在类中，受类域限制
		static Singleton *_sint;//静态全局对象放进类域里


		class GC//类似于RAII
		{
		public:
			~GC()
			{
				Singleton::DelInstance();
			}
		};
		static GC gc;
	};
	Singleton::GC Singleton::gc;
	Singleton* lazy::Singleton::_sint=nullptr;
};