 #define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

#include"Date.h"

int main()
{
	Date d1(2024, 1, 29);
	Date d2 = d1 + 20;
	d2.Print();
	d1.Print();

	d2 -= 20;
	d2.Print();

	d1 += 30000;
	d1.Print();

	++d1;
	d1.operator++();
	d1.Print();

	d1++;
	d1.operator++(10);
	d1.Print();

	/*bool ret = false;
	if (ret)
	{
		d1.Print();
	}*/

	Date d4(2024, 1, 29);
	Date d5(2024, 8, 1);
	cout << d5 - d4 << endl;

	return 0;
}