#define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <thread>
#include <memory>

#pragma comment(lib, "Ws2_32.lib")

static bool isrunning = false;
static std::string Clientname;

class Client
{
public:
    Client(const std::string& server_ip, uint16_t server_port)//获取用户ip 链接端口
    {
        WSADATA wsaData;
        int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (result != 0)
        {
            std::cerr << "WSAStartup failed: " << result << std::endl;
            exit(EXIT_FAILURE);
        }

        _sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sockfd == INVALID_SOCKET)
        {
            std::cerr << "socket creation failed: " << WSAGetLastError() << std::endl;
            WSACleanup();
            exit(EXIT_FAILURE);
        }
        //socket初始化
        memset(&_serverAddr, 0, sizeof(_serverAddr));
        _serverAddr.sin_family = AF_INET;
        _serverAddr.sin_port = htons(server_port);//本地端口绑定
        if (inet_pton(AF_INET, server_ip.c_str(), &_serverAddr.sin_addr) <= 0)//将IP地址从字符串形式转换为二进制形式。
        {
            std::cerr << "inet_pton failed: " << WSAGetLastError() << std::endl;
            closesocket(_sockfd);//关闭套接字
            WSACleanup();
            exit(EXIT_FAILURE);
        }

        _isrunning = true;
    }

    void start()
    {
        std::cout << "你是? ";
        std::cin >> _clientName;
        std::cout << "可以进行通信了!" << std::endl;

        std::thread recvThread(&Client::receive, this);
        std::thread sendThread(&Client::send, this);

        recvThread.detach();
        sendThread.detach();//线程分离

        while (_isrunning)
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }

        closesocket(_sockfd);
        WSACleanup();
    }

private:
    void receive()
    {
        while (_isrunning)
        {
            char buffer[1024] = { 0 };
            struct sockaddr_in peer;
            int peerlen = sizeof(peer);
            std::cout << "server: " ;
            int n = recvfrom(_sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr*)&peer, &peerlen);
            if (n > 0)
            {
                buffer[n] = 0;
                std::cout <<buffer << std::endl;
            }
            else if (n == SOCKET_ERROR)
            {
                std::cerr << "recvfrom error: " << WSAGetLastError();
            }
        }
    }

    void send()
    {
        while (_isrunning)
        {
            std::cout << _clientName << ": ";
            std::string message;
            std::cin >> message;

            int sent = sendto(_sockfd, message.c_str(), message.size(), 0, (struct sockaddr*)&_serverAddr, sizeof(_serverAddr));
            if (sent == SOCKET_ERROR)
            {
                std::cerr << "sendto error: " << WSAGetLastError() << std::endl;
            }
            std::cout << std::endl;
        }
    }

private:
    SOCKET _sockfd;
    struct sockaddr_in _serverAddr;
    bool _isrunning;
    std::string _clientName;
};

void useagge(const std::string& proc)
{
    std::cout << "Usage:\n\t" << proc << " serverip serverport\n"
        << std::endl;
}

int main()
{
    std::string serverip;
    std::string portStr;
    std::cout << "输入服务器ip: ";
    std::cin >> serverip;
    std::cout << "输入服务器端口号: ";
    std::cin >> portStr;

    uint16_t serverport;
    try
    {
        serverport = static_cast<uint16_t>(std::stoi(portStr));
    }
    catch (const std::exception& e)
    {
        std::cerr << "无效的端口号: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    std::unique_ptr<Client> csvr = std::make_unique<Client>(serverip, serverport); // C++14
    csvr->start();
    return 0;
}
