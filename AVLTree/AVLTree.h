#pragma once
#include <iostream>
#include <utility>
#include<assert.h>
using namespace std;

template<class K,class V>
struct TreeNode
{
	TreeNode<K,V>* _left;
	TreeNode<K,V>* _right;
	TreeNode<K,V>* _parent;
	pair<K, V> _kv;
	int _count;//计数
	TreeNode(const pair<K,V>& kv)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_kv(kv)
		,_count(0)//平衡因子
	{
	}
};
template<class K,class V>
class AVL
{
public:
	typedef TreeNode<K, V> Node;
	bool insert(const pair<K, V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;//找
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}//找点
		cur = new Node(kv);
		if (parent->_kv.first < kv.first)
		{
			parent->_right = cur;
		}
		else
		{
			parent->_left = cur;
		}
		cur->_parent = parent;
		while (parent)//parent为空结束
		{
			if (parent->_count == 0)//平衡因子为0就终止
			{
				break;
			}
			if (cur == parent->_left)
			{
				parent->_count--;//左-
			}
			else if(cur==parent->_right)
			{
				parent->_count++;//右+
			}
		
			else if (parent->_count == 1 || parent->_count == -1)//为1继续向上迭代
			{
				cur = cur->_parent;
				parent = parent->_parent;//向上迭代
			}
			else if (parent->_count == 2 || parent->_count == -2)//为2就要旋转了 让其保持平衡 保持AVL特性
			{
				if (parent->_count == 2 && cur->_count == 1)
					RotateL(parent);
				else if (parent->_count == -2 && cur->_count == -1)
					RotateR(parent);
				else if (parent->_count == -2 && cur->_count == 1)
					RotateLR(parent);
				else
				{
					RotateRL(parent);
				}
			}
			else
			{
				assert(false);
			}
		}
		return true;
	}
	void inorder()
	{
		_Inorder(_root);
	}
private:
	void _Height(Node* root)
	{
		int L = 1 + _Height(root->_left);
		int R = 1 + _Height(root->_right);
		return L > R ? L : R;
	}
	void _Inorder(Node* root)
	{
		if (root == nullptr)
			return;
		_Inorder(root->_left);
		cout <<root->_kv.first<<":" << root->_kv.second << " " << endl;
		_Inorder(root->_right);
	}
	//画图
	void RotateL(Node* parent)//左旋调整
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		Node* ppnode = parent->_parent;
		parent->_parent = subR;

		if (ppnode->_left == parent)
		{
			ppnode->_left = subR;
		}
		else if (ppnode->_right == parent)
		{
			ppnode->_right == subR;
		}
		else
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		parent->_count = 0;
		subR->_count = 0;
	}
	void RotateR(Node* parent)//左旋调整
	{
		Node* subL = parent->_left;
  		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		subL->_right = parent;

		Node* ppnode = parent->_parent;
		parent->_parent = subL;

		if (parent == _root)//为root情况
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else//分情况讨论
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subL;
			}
			else
			{
				ppnode->_right = subL;
			}
			subL->_parent = ppnode;
		}
		
		parent->_count = 0;
		subL->_count = 0;
	}
	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		
		int bf = subLR->_count;
		RotateL(parent->_left);
		RotateR(parent);
		//重点在调整平衡因子
		if (bf == -1)//调整平衡因子
		{
			subLR->_count = 0;
			subL->_count = 0;
			parent->_count = 1;
		}
		else if (bf == 1)
		{
			subLR->_count = 0;
			subL->_count = -1;
			parent->_count = 1;
		}
		else if (bf == 0)
		{
			subLR->_count = 0;
			subL->_count = 0;
			parent->_count = 0;
		}
		else
		{
			assert(false);
		}
	}
	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;


		int bf = subRL->_count;

		RotateR(subR);
		RotateL(parent);
		//重点在调整平衡因子
		if (bf == -1)//调整平衡因子
		{
			subRL->_count = 0;
			subR->_count = 0;
			parent->_count = 1;
		}
		else if (bf == 1)
		{
			subRL->_count = 0;
			subR->_count = -1;
			parent->_count = 1;
		}
		else if (bf == 0)
		{
			subRL->_count = 0;
			subR->_count = 0;
			parent->_count = 0;
		}
		else
		{
			assert(false);
		}
	}
	Node* _root = nullptr;
};