#pragma once
#include <iosfwd>
#include <iostream>
#include <fstream>
#include <thread>
#include <string>
#include <cstring>
#include <stdarg.h>
#include <Windows.h>
#include <ctime>
#include <mutex>
namespace LOG{

	std::string logfile = "log.txt";
	bool gIsSave = false;

	std::mutex _lock;

	enum level
	{
		DEBUG = 0,
		INFO,
		WARNING,
		ERRORR,
		FATAL
	};

	std::string LevelToString(int level)
	{
		switch (level)
		{
		case DEBUG:
			return "DEBUG";
		case INFO:
			return "INFO";
		case WARNING:
			return "WARNING";
		case ERRORR:
			return "ERROR";
		case FATAL:
			return "FATAL";

		default:
			return "UNKOWN";
		}
	}
	std::string Gettime()
	{
		time_t currtime = time(nullptr);
		struct tm* NowTime = localtime(&currtime);
		if (NowTime == nullptr)
		{
			return "ERROR";
		}
		char buffer[1024];
		snprintf(buffer, sizeof(buffer), "%d-%d-%d-%d-%d-%d",
			NowTime->tm_year,
			NowTime->tm_mon,
			NowTime->tm_wday,
			NowTime->tm_hour,
			NowTime->tm_min,
			NowTime->tm_sec
		);
		return buffer;
	}

	void Savefile(std::string& filename, std::string& message)
	{
		std::ofstream out(filename, std::ios::app);
		if (!out.is_open())
		{
			return;
		}
		out << message;
		out.close();
	}

	void Logmessage(int level, int line, std::string filename, const char* format, ...)
	{
		std::string clevel = LevelToString(level);
		std::string time = Gettime();

		va_list arg;
		char buffer[1024];
		va_start(arg, format);
		vsnprintf(buffer, sizeof(buffer), format, arg);
		va_end(arg);
		int selfpid = GetCurrentProcessId();
		std::string message = "[" + time + "]" + "[" + clevel + "]" +
			"[" + std::to_string(selfpid) + "]" +
			"[" + filename + "]" + "[" + std::to_string(line) + "] " + buffer + "\n";
		std::lock_guard<std::mutex> guard(_lock);
		if (!gIsSave)
		{
			std::cout << message << std::endl;
		}
		else
		{
			Savefile(filename, message);
		}
	}

#define LOG(level,format,...)  do{ Logmessage(level,__LINE__,format,,##__VA_ARGS__);}while(0)
#define Saveinfile do{gIsSave=true;}while(0)
#define Saveinsrceem do{gIsSave=false;}while(0)




}