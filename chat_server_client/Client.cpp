#define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <thread>
#include <memory>
#include <mutex>
#include <functional>
#include <Windows.h>
#include <locale>
#include <codecvt>

#pragma warning(disable:4996)

#pragma comment(lib, "Ws2_32.lib")

static bool isrunning = false;
static std::string Clientname;

using func_t = std::function<void(std::string)>;//回调


class Client
{
public:
    Client(const std::string& server_ip, uint16_t server_port)//获取用户ip 链接端口
    {
        WSADATA wsaData;
        int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (result != 0)
        {
            std::cerr << "WSAStartup failed: " << result << std::endl;
            exit(EXIT_FAILURE);
        }

        _sockfd = socket(AF_INET, SOCK_DGRAM, 0);//SOCKET 文件描述符封装
        if (_sockfd ==SOCKET_ERROR)
        {
            std::cerr << "socket creation failed: " << WSAGetLastError() << std::endl;
            exit(EXIT_FAILURE);
        }
        //socket初始化
        memset(&_serverAddr, 0, sizeof(_serverAddr));
        _serverAddr.sin_family = AF_INET;
        _serverAddr.sin_port = htons(server_port);//本地端口绑定
        inet_pton(AF_INET, server_ip.c_str(), &_serverAddr.sin_addr);
        std::cout << "你是? ";
        std::cin >> _clientName;

        _isrunning = true;
        count = 0;
        _mutex.lock();
    }

    void start()
    {
        std::cout << "可以进行通信了!" << std::endl;
        std::thread recv_thread(&Client::receive, this);
        std::thread send_thread(&Client::send, this);

        recv_thread.detach();
        send_thread.detach();

        while (_isrunning)
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
    ~Client()
    {
        closesocket(_sockfd);
        WSACleanup();
    }
private:
    void receive()
    {
        while (_isrunning)
        {
            while (_isrunning)
            {
                char buffer[1024] = { 0 };
                struct sockaddr_in peer;
                int peerlen = sizeof(peer);
                int n = recvfrom(_sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr*)&peer, &peerlen);
                if (n > 0)
                {
                    buffer[n] = 0;
                    _mutex.unlock();
                        fprintf(stderr, "[other ]| %s\n", buffer);
                        fflush(stderr);
                    _mutex.lock();
                }          
            }
        }
    }

    void send()
    {
        while (_isrunning)
        {
            if (_sockfd == INVALID_SOCKET)
            {
                std::cerr << "Invalid socket" << std::endl;
                return;
            }
            _mutex.unlock();
            if (count == 0) {
                printf("%s|Enter", _clientName.c_str());
                count = 1;
            }
            _mutex.lock();
            fflush(stdout);
            fflush(stderr);

            std::string message;
            std::getline(std::cin, message);

            // Convert message to UTF-8
            std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
            std::string utf8_message = converter.to_bytes(std::wstring(message.begin(), message.end()));

            int result = sendto(_sockfd, utf8_message.c_str(), utf8_message.size(), 0, (struct sockaddr*)&_serverAddr, sizeof(_serverAddr));
            if (result == SOCKET_ERROR)
            {
                std::cerr << "sendto error: " << WSAGetLastError() << std::endl;
            }
        }
    }

private:
    SOCKET _sockfd;
    struct sockaddr_in _serverAddr;
    bool _isrunning;
    std::string _clientName;
    std::mutex _mutex;
    int count;
};

void useagge(const std::string& proc)
{
    std::cout << "Usage:\n\t" << proc << " serverip serverport\n"
        << std::endl;
}

int main()
{
    std::string serverip="120.78.131.92";
    std::string portStr="8088";


    uint16_t serverport;
    try
    {
        serverport = static_cast<uint16_t>(std::stoi(portStr));
    }
    catch (const std::exception& e)
    {
        std::cerr << "无效的端口号: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    std::unique_ptr<Client> csvr = std::make_unique<Client>(serverip, serverport); // C++14
    csvr->start();
    return 0;
}